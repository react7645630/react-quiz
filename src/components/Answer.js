const Answer = ({ answerText, 
    onSelectAnswer, 
    index, 
    currentAnswer, 
    correctAnswer,
}) => {    /*ubacili smo argument answerText iz Question.js*/
const letterMapping = ["A", "B", "C", "D"];
const isCorrectAnswer = currentAnswer && answerText === correctAnswer;
const isWrongAnswer = currentAnswer === answerText && answerText !== correctAnswer;
const correctAnswerClass = isCorrectAnswer ? "correct-answer" : "";        /*ovo "correct-answer" to je iz css i on odje kaze ako je correctAnswer tacno da ovo pozeleni(sredjeno prethodno u css)*/
const wrongAnswerClass = isWrongAnswer ? "wrong-answer" : "";
const disableClass = currentAnswer ? "disabled-answer" : "";        /*a ovo je za odgovre koji niti crvene niti pozelene, to su od ako kliknemo npr tacno ostali odg su disabled*/
    return (       /*dolje smo stavili {letterMapping[index]} ovo index nam odvaja slova pojedinacno, da ne stoje ispred pola ABCD*/
    <div className={`answer ${correctAnswerClass} ${wrongAnswerClass} ${disableClass}`} 
    onClick={() => onSelectAnswer(answerText)}>  
        <div className="answer-letter">{letterMapping[index]}</div>     
        <div className="answer-text">{answerText}</div>
    </div>
    );
};

export default Answer; 