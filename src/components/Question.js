import { useContext } from "react";
import Answer from "./Answer";
import { QuziContext } from "../contexts/quiz";

const Question = () => {
    const [quizState, dispatch] = useContext(QuziContext)
    const currentQuestion = quizState.questions[quizState.currentQuestionIndex];  /*ovo currentQuestion je pitanje, tacan, netacan odgovor. I to smo ubacili dolje u div question, a to su pitanja koja se mijenjaju*/
    return (
    <div>                                
        <div className="question">{currentQuestion.question}</div> 
        <div className="answers">     
           {quizState.answers.map((answer, index) => (             /*ovdje koristimo metodu map na quiz.answers, i za svaki element answer. Zatim se <Answer /> dodjeljuje answer i svojstvo answerText*/
            <Answer 
            answerText={answer} 
            key={index} 
            index={index}
            currentAnswer={quizState.currentAnswer}
            correctAnswer={currentQuestion.correctAnswer}
            onSelectAnswer={(answerText) => dispatch({type: "SELECT_ANSWER", payload: answerText})} />   /*definise se onSelectAnswer funkcija koja kada korisnik izabere odgovor salje akciju "SELECT_ANSWER" sa answerText kao payload(sa njim se prenose podaci koje zelimo da prenesemo zajedno sa akcijom, ovdje je to tekst odgovora na koji smo kliknuli)*/
           ))}
        </div>
    </div>
    );
};

export default Question; 