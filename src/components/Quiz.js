import { useContext, useEffect } from "react";
import Question from "./Question";
import { QuziContext } from "../contexts/quiz";


const Quiz = () => {
    const [quizState, dispatch] = useContext(QuziContext);
    const apiUrl = "https://opentdb.com/api.php?amount=10&category=31&difficulty=easy&type=multiple&encode=url3986"

    useEffect(() => {
        fetch(apiUrl)
        .then(res => res.json())
        .then(data => {
            dispatch({type: "LOADED_QUESTIONS", payload: data.results});
        });
    }, []);
    return (
    <div className="quiz">
        {quizState.showResults && (    /*a ako je quizState.showResults tacan onda ce se poslije pitanja otvoriti novi div congratulations*/
            <div className="results">
                <div className="congratulations">Congratulations</div>
                <div className="results-info">
                    <div>You have completed the quiz.</div>
                    <div>
                        You' ve got {quizState.correctAnswersCount} of {""}
                        {quizState.questions.length}</div>
                </div>
                <div className="next-button" onClick={() => dispatch ({type: "RESTART"})}>Restart</div>
            </div>
        )}
        {!quizState.showResults && quizState.questions.length > 0 && (   /*sa ovim smo stavili da ako nije tacno quizState.showResult da ocitava(renduje) ovo dolje, tacnije ocita se score ili redni broj pitanja, pitanje i dugme za next question*/
        <div>     
            <div className="score">         
                Question {quizState.currentQuestionIndex + 1}/
                {quizState.questions.length}    
            </div> 
            <Question />      
            <div                               /*ovdje smo napravili da na klik dugmeta 'dispatch funkcija prima stanje akcije NEXT_QUESTION'*/
             className="next-button"
             onClick={() => dispatch({type: "NEXT_QUESTION"})}> 
             Next-question
            </div>
        </div>
        )}
    </div>
    );
};

export default Quiz;          /*export default znaci da izvozi samo jedan element, i da tamo prilikom importa ne koristimo {} ove zagrade*/