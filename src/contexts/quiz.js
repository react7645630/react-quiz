import { createContext, useReducer } from "react";
import { normalizeQuestions, shuffleAnswers } from "../helpers";

const initialState = {
    currentQuestionIndex: 0,
    questions: [],                 /*ovo je question odozgo, a to smo importovali iz data.js a to su pitanja koja se nalaze tamo*/
    showResults: false,
    answers: [],
    currentAnswer: "",
    correctAnswersCount: 0,                   /*zbir tacnih odgovora*/
};

const reducer = (state, action) => {    /*ovaj reduktor prima trenurno stanje i akciju*/
    switch (action.type) {                   /*switch obradjuje vrste akcija, npr u ovom slucaju action.type je jednak "NEXT_QUESTION" tako da ce se izvrsiti blok koda unutar tog 'case'*/
        case "SELECT_ANSWER": {
            const correctAnswersCount = 
            action.payload === 
            state.questions[state.currentQuestionIndex].correctAnswer 
            ? state.correctAnswersCount + 1 
            : state.correctAnswersCount;
            return {
                ...state,
                currentAnswer: action.payload,
                correctAnswersCount,
            };
        }
        case "NEXT_QUESTION": {
            const showResults = state.currentQuestionIndex === state.questions.length - 1;  /*LOGIKA ZA DUGME NEXT QUESTION..ovjde pita da li je trenutrni index pitanja jednak poslednjem indeksu pitanja (-1)*/
            const currentQuestionIndex = showResults    /*Kreira se promenljiva currentQuestionIndex koja se postavlja na trenutni indeks pitanja ako je showResults tačno, u suprotnom se postavlja na trenutni indeks pitanja uvećan za 1.*/
              ? state.currentQuestionIndex 
              : state.currentQuestionIndex + 1;
            const answers = showResults  /*odje se const answers dodjeljuje ako je showResult tacno(?) da vrati prazan niz, a ako je netacno(:) tada se poziva funkcija shuffleAnswres da bi se izmijesali odgovori i vratili promjenjivoj answers*/
            ? [] 
            : shuffleAnswers(state.questions[currentQuestionIndex]);
          return {
              ...state,
              currentQuestionIndex,
              showResults,
              answers,
              currentAnswer: "",          /*ovo smo stavili da prilikom klika next question, sledeci odgovor ne pozeleni sam, vec da se vrate svi odgovri bez boje*/
              };
        }
        case "RESTART": {
            return initialState;
        }
        case "LOADED_QUESTIONS": {
            const normalizedQuestions = normalizeQuestions(action.payload);
            return {
                ...state, 
                questions: normalizedQuestions, 
                answers: shuffleAnswers(normalizedQuestions[0]),
            }
        }
        default: {
            return state;     /*ako ne onda samo vrace trenutno stanje*/
        }
    }
};

export const QuziContext = createContext()

export const QuizProvider = ({children}) => {                    /*ovo {children} je <Quiz/> u index.js, i moramo ga postaviti jer se tamo nalazi quiz komponenta unutar ove quizprovider komponente */
    const value = useReducer(reducer, initialState);
    return <QuziContext.Provider value = {value}>{children}
    </QuziContext.Provider>;   /*a odje se samo ubaci {children} da bi sve radilo*/
}