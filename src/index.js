import React from "react";
import { createRoot } from "react-dom/client";
import "./index.css"               /*globalni css */
import Quiz from "./components/Quiz";
import { QuizProvider } from "./contexts/quiz";

createRoot(document.getElementById('root'))
.render(
  <React.StrictMode>
    <QuizProvider>
    <Quiz />                    
    </QuizProvider> 
  </React.StrictMode>,
);

