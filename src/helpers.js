export const shuffleAnswers = question => {
    const unshuffledAnswers = [
        question.correctAnswer,
        ...question.incorrectAnswers,
      ];     /*ove tri tacke predstavljaju sirenje operatora, tj spajamo tacan i netacne odg u jedan niz*/
    
    return unshuffledAnswers
    .map((unshuffledAnswer) => ({          /*odje koristimo map metodu da nam svaki odgovor ima nasumican broj i svojstvo value sa odgovorom*/
        sort: Math.random(),
        value: unshuffledAnswer,
    }))
    .sort((a, b) => a.sort - b.sort)            /*na kraju korsitimo sortiranje odgovora po rastucem redosledu prema sort, odnosno prema gore brojevima(a.sort - b.sort), i */
    .map((a) => a.value);        /*Kao rezultat, dobija se novi niz koji sadrži samo vrednosti value iz sortiranog niza objekata*/
};

export const normalizeQuestions = (backendQuestions) => {
    return backendQuestions.map((backendQuestion) => {
        const incorrectAnswers = backendQuestion.incorrect_answers.map(
            (incorrectAnswer) => decodeURIComponent(incorrectAnswer)
          );
        return {
            correctAnswer: decodeURIComponent(backendQuestion.correct_answer),
            question: decodeURIComponent(backendQuestion.question),
            incorrectAnswers,
        };
    });
};